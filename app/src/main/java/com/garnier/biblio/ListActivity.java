package com.garnier.biblio;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Collu Johan on 17/02/2016.
 **/
public class ListActivity extends Activity {

    private LivresBDD bd;
    private ListView listView;
    private ArrayList<Integer> selected_items = new ArrayList<Integer>();
    ArrayList<Livre> biblio;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_suppr);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        bd = new LivresBDD(this);
        biblio = getBiblio();

        listView = (ListView) findViewById(R.id.listView);

        ArrayList<String> titres = new ArrayList<String>();

        for(Livre l : biblio){
            titres.add(l.getTitre());
        }

        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.listcontentsupp, titres);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(selected_items.contains(position)) selected_items.remove((Integer) position);
                else selected_items.add(position);
            }
        });

        findViewById(R.id.imageButtonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.buttonSuppr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( selected_items.size() > 0 ) {
                    findViewById(R.id.relativeLayoutPopup).setVisibility(View.VISIBLE);
                    findViewById(R.id.buttonYes).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openBD();
                            for (Integer i : selected_items) {
                                bd.removeLivreWithID(biblio.get(i).getId());
                                biblio.remove((int) i);
                            }
                            closeBD();

                            ArrayList<String> titres = new ArrayList<String>();

                            for(Livre l : biblio){
                                titres.add(l.getTitre());
                            }

                            ArrayAdapter adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.listcontentsupp, titres);
                            listView.setAdapter(adapter);

                            findViewById(R.id.relativeLayoutPopup).setVisibility(View.GONE);
                        }
                    });
                    findViewById(R.id.buttonNo).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            findViewById(R.id.relativeLayoutPopup).setVisibility(View.GONE);
                        }
                    });
                }else Toast.makeText(getApplicationContext(), "Vous ne pouvez supprimer 0 livre", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private ArrayList<Livre> getBiblio(){
        openBD();
        ArrayList<Livre> res = bd.getBiblio();
        closeBD();
        return res;
    }

    private void openBD(){ bd.open(); }
    private void closeBD(){ bd.close(); }

}
