package com.garnier.biblio;
 
/**
 * Création d'un livre tout simple pour un exemple d'utilisation de SQLite sous Android
 * @author Axon
 * http://www.tutomobile.fr
 */
public class Livre {
 
	private int id;
	private String isbn, titre, auteur;
 
	public Livre(){}

	public Livre(String isbn, String titre, String auteur){
		this.isbn = isbn;
		this.titre = titre;
		this.auteur = auteur;
	}

	public Livre(int id, String isbn, String titre, String auteur) {
		this.id = id;
		this.isbn = isbn;
		this.auteur = auteur;
		this.titre = titre;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
 
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
 
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAuteur(){ return auteur; }
	public void setAuteur(String auteur){ this.auteur = auteur;	}

	public String toString(){
		return "ID : "+id+"\nISBN : "+isbn+"\nTitre : "+titre;
	}
}