package com.garnier.biblio;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Biblio extends Activity implements View.OnClickListener {

    private LivresBDD bd;
    private ArrayList<Livre> biblio;
    private Livre livre = null;
    private EditText editTextIsbn, editTextTitre, editTextAuteur;
    private TextView count, textViewPosition;
    private int position = 0;
    private String isbn, titre, auteur;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        findViewById(R.id.buttonPrev).setOnClickListener(this);
        findViewById(R.id.buttonNext).setOnClickListener(this);
        findViewById(R.id.buttonSearch).setOnClickListener(this);
        findViewById(R.id.buttonAdd).setOnClickListener(this);
        findViewById(R.id.buttonUpdate).setOnClickListener(this);
        findViewById(R.id.buttonDelete).setOnClickListener(this);
        editTextIsbn = (EditText) findViewById(R.id.editTextISBN);
        editTextTitre = (EditText) findViewById(R.id.editTextTitre);
        editTextAuteur = (EditText) findViewById(R.id.editTextAuteur);

        bd = new LivresBDD(this);

        biblio = getBiblio();

        count = (TextView) findViewById(R.id.textViewCount);
        count.setText(String.valueOf(biblio.size()));
        textViewPosition = (TextView) findViewById(R.id.textViewPosition);

        if( biblio.size() > 0 ) livre = biblio.get(0);
        else{
            textViewPosition.setText("0");
            position = -1;
        }

        if( livre  != null ){
            String titre = getIntent().getStringExtra("choice");
            if( titre != null ){
                livre = getLivreByTitre(titre);
                for(int i = 0; i < biblio.size(); i++){
                    if(biblio.get(i).getTitre().equals(titre)) {
                        position = i;
                        break;
                    }
                }
                textViewPosition.setText(String.valueOf(position));
            }
            if( getIntent().getStringExtra("add") != null){
                position = biblio.size();
                textViewPosition.setText(String.valueOf(position));
                editTextIsbn.setText("");
                editTextTitre.setText("");
                editTextAuteur.setText("");
            }else{
                editTextIsbn.setText(livre.getIsbn());
                editTextTitre.setText(livre.getTitre());
                editTextAuteur.setText(livre.getAuteur());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.scan:
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
                startActivityForResult(intent, 0);
                return true;
            case R.id.leave:
                setResult(1);
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0){
            String contents = data.getStringExtra("SCAN_RESULT");
            Toast.makeText(getApplicationContext(), contents, Toast.LENGTH_SHORT).show();
            editTextIsbn.setText(contents);

            if( isOnline()) {
                String url = "https://www.googleapis.com/books/v1/volumes?q=" + contents;
                String[] urls = { url };
                new JSONPARSER().execute(urls);
            }else{
                Toast.makeText(getApplicationContext(), "Pas de connexion internet !", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void openBD(){ bd.open(); }
    private void closeBD(){ bd.close(); }

    private void getPrev(){
        position--;
        textViewPosition.setText(String.valueOf(position));
        livre = biblio.get(position);
        setEditText();
    }

    private void getNext(){
        position++;
        textViewPosition.setText(String.valueOf(position));
        livre = biblio.get(position);
        setEditText();
    }

    private void setEditText(){
        if( livre != null) {
            editTextIsbn.setText(livre.getIsbn());
            editTextTitre.setText(livre.getTitre());
            editTextAuteur.setText(livre.getAuteur());
        }else{
            editTextIsbn.setText("");
            editTextTitre.setText("");
            editTextAuteur.setText("");
        }
    }

    private ArrayList<Livre> getBiblio(){
        openBD();
        ArrayList<Livre> res = bd.getBiblio();
        closeBD();
        return res;
    }

    private void addLivreToBD(String isbn, String titre, String auteur){
        openBD();
        Livre l = new Livre(isbn, titre, auteur);
        bd.insertLivre(l);
        livre = getLivreByTitre(titre);
        biblio.add(livre);
        position = biblio.size()-1;
        textViewPosition.setText(String.valueOf(position));
        count.setText(String.valueOf(biblio.size()));
        closeBD();
    }

    private Livre getLivreByTitre(String titre){
        openBD();
        Livre res = bd.getLivreWithTitre(titre);
        for(int i = 0; i < biblio.size(); i++){
            if(biblio.get(i).getTitre().contains(titre)) {
                position = i;
                break;
            }
        }
        closeBD();
        return res;
    }

    private void updateLivre(Livre livre){
        openBD();
        bd.updateLivre(livre.getId(), livre);
        closeBD();
    }

    private void removeLivre(int id){
        openBD();
        bd.removeLivreWithID(id);
        closeBD();
        biblio.remove(livre);
        if( position == 0 && biblio.size() != 0 ){
            livre = biblio.get(position);
        }else if( biblio.size() != 0 ){
            position--;
            livre = biblio.get(position);
            textViewPosition.setText(String.valueOf(position));
        }else{
            position--;
            livre = null;
            textViewPosition.setText(String.valueOf(position));
        }
        count.setText(String.valueOf(biblio.size()));
        setEditText();
    }

    private boolean verif(){
        isbn = editTextIsbn.getText().toString();
        titre = editTextTitre.getText().toString();
        auteur = editTextAuteur.getText().toString();
        if( !isbn.equals("") && !titre.equals("") && !auteur.equals("")){
            deleteError();
            return true;
        }else{
            if(isbn.equals(""))
                editTextIsbn.setError("Veuillez saisir un isbn !");
            if(titre.equals(""))
                editTextTitre.setError("Veuillez saisir un titre !");
            if(auteur.equals(""))
                editTextAuteur.setError("Veuillez saisir un auteur !");
            return false;
        }
    }

    private boolean verifTitre(){
        if( !editTextTitre.getText().toString().equals("")){
            deleteError();
            return true;
        }else{
            editTextTitre.setError("Veuillez saisir un titre !");
            return false;
        }
    }

    private void deleteError(){
        editTextIsbn.setError(null);
        editTextTitre.setError(null);
        editTextAuteur.setError(null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonPrev:
                if( biblio.size() == 0) Toast.makeText(getApplication(), "La bibliothéque ne contient pas de livre !", Toast.LENGTH_SHORT).show();
                else if( position > 0 ) getPrev();
                else Toast.makeText(getApplication(), "Pas de précédent !", Toast.LENGTH_SHORT).show();
                break;
            case R.id.buttonNext:
                if( biblio.size() == 0) Toast.makeText(getApplication(), "La bibliothéque ne contient pas de livre !", Toast.LENGTH_SHORT).show();
                else if( position + 1 < biblio.size()) getNext();
                else Toast.makeText(getApplication(), "Pas de suivant !", Toast.LENGTH_SHORT).show();
                break;
            case R.id.buttonSearch:
                deleteError();
                if( verifTitre() ) {
                    livre = getLivreByTitre(editTextTitre.getText().toString());
                    if( livre != null ) {
                        editTextIsbn.setText(livre.getIsbn());
                        editTextAuteur.setText(livre.getAuteur());
                        textViewPosition.setText(String.valueOf(position));
                    }else{
                        Toast.makeText(getApplication(), "La bibliothéque ne contient pas de livre répondant à votre recherche !", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.buttonAdd:
                deleteError();
                if( verif() ){
                    addLivreToBD(isbn, titre, auteur);
                    Toast.makeText(getApplicationContext(), "Livre ajouté", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.buttonDelete:
                if( livre != null)  removeLivre( livre.getId() );
                else Toast.makeText(getApplication(), "La bibliothéque ne contient pas de livre !", Toast.LENGTH_SHORT).show();
                break;
            case R.id.buttonUpdate:
                deleteError();
                if( livre != null ){
                    if( verif() ) {
                        livre.setIsbn(editTextIsbn.getText().toString());
                        livre.setTitre(editTextTitre.getText().toString());
                        livre.setAuteur(editTextAuteur.getText().toString());
                        updateLivre(livre);
                        Toast.makeText(getApplication(), "Mise à jour effectué !", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplication(), "La bibliothéque ne contient pas de livre !", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    class JSONPARSER extends AsyncTask<String, Void, Void> {

        private ProgressDialog prgDialog;
        JSONObject jsonObject = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* prgDialog = new ProgressDialog(Biblio.this);
            prgDialog.setCancelable(false);
            prgDialog.setIndeterminate(true);
            prgDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            prgDialog.show();*/
        }

        @Override
        protected Void doInBackground(String... Url) {
            try {
                DefaultHttpClient defaultClient = new DefaultHttpClient();
                HttpGet httpGetRequest = new HttpGet( Url[0] );
                HttpResponse httpResponse = defaultClient.execute(httpGetRequest);
                BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));

                StringBuilder sBuilder = new StringBuilder();

                String json;
                String line;
                while ((line = reader.readLine()) != null) {
                    sBuilder.append(line + "\n");
                }
                json = sBuilder.toString();
                jsonObject = new JSONObject(json);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                JSONObject livres = jsonObject.getJSONArray("items").getJSONObject(0);
                JSONObject info = new JSONObject(livres.getString("volumeInfo"));
                titre = info.getString("title");
                String authors = info.getJSONArray("authors").get(0).toString();
                Log.e("auteur",info.getJSONArray("authors").get(0).toString());
                auteur = authors;
            } catch (JSONException e) {
                e.printStackTrace();
            }


            editTextTitre.setText(titre);
            editTextAuteur.setText(auteur);

            //prgDialog.dismiss();
        }
    }


}