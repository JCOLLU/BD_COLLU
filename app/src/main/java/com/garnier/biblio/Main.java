package com.garnier.biblio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.*;

/**
 * Created by Johan Collu on 12/01/16.
 **/
public class Main extends Activity implements View.OnClickListener{

    private LivresBDD bd;
    private ArrayList<Livre> biblio;
    private ListView listView;
    private ArrayList<HashMap<String, String>> listItem;
    private HashMap<String, String> map;
    private MyListAdapter mSchedule;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        bd = new LivresBDD(this);

        biblio = getBiblio();

        listView = (ListView) findViewById(R.id.listView);

        listItem = new ArrayList<HashMap<String, String>>();

        for(Livre l : biblio){
            map = new HashMap<String, String>();
            map.put("Titre", l.getTitre());
            map.put("Auteur", l.getAuteur());
            listItem.add(map);
        }

        mSchedule = new MyListAdapter(this, listItem,
                R.layout.listecontent, new String[]{"Titre", "Auteur"}, new int[]{
                R.id.textViewTitre, R.id.textViewAuteur
        });
        listView.setAdapter(mSchedule);
        listView.setOnItemClickListener(listenerLivre);

        findViewById(R.id.buttonByTitle).setOnClickListener(this);
        findViewById(R.id.buttonByAuteur).setOnClickListener(this);
        findViewById(R.id.buttonByIsbn).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i;
        switch (item.getItemId()) {
            case R.id.new_livre:
                i = new Intent(this, Biblio.class);
                i.putExtra("add", "add");
                startActivityForResult(i, 1);
                return true;
            case R.id.leave:
                finish();
                return true;
            case R.id.supp_livre:
                i = new Intent(this, ListActivity.class);
                startActivityForResult(i, 1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private AdapterView.OnItemClickListener listenerLivre = new AdapterView.OnItemClickListener() {

        public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
            if(view.findViewById(R.id.linearLayoutInfo).getVisibility() == View.VISIBLE) {
                view.findViewById(R.id.linearLayoutInfo).setVisibility(View.INVISIBLE);
                view.findViewById(R.id.linearLayoutButton).setVisibility(View.VISIBLE);
                view.findViewById(R.id.buttonListUpdate).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    Intent i = new Intent(Main.this, Biblio.class);
                    i.putExtra("choice", biblio.get(position).getTitre());
                    startActivityForResult(i,1000);
                    }
                });
                view.findViewById(R.id.buttonListDelete).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    openBD();
                    bd.removeLivreWithID(biblio.get(position).getId());
                    closeBD();
                    biblio.remove(biblio.get(position));
                    listItem = new ArrayList<HashMap<String, String>>();
                    for(Livre l : biblio){
                        map = new HashMap<String, String>();
                        map.put("Titre", l.getTitre());
                        map.put("Auteur", l.getAuteur());
                        listItem.add(map);
                    }
                    mSchedule = new MyListAdapter(getApplicationContext(), listItem,
                            R.layout.listecontent, new String[]{"Titre", "Auteur"}, new int[]{
                            R.id.textViewTitre, R.id.textViewAuteur
                    });
                    listView.setAdapter(mSchedule);
                    }
                });
                view.findViewById(R.id.buttonListBack).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        view.findViewById(R.id.linearLayoutInfo).setVisibility(View.VISIBLE);
                        view.findViewById(R.id.linearLayoutButton).setVisibility(View.GONE);
                    }
                });
                view.findViewById(R.id.buttonListDisplay).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = "http://books.google.fr/books?isbn=";
                        url += biblio.get(position).getIsbn();
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                    }
                });
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.buttonByTitle:
                Collections.sort(biblio, new Comparator<Livre>() {
                    @Override
                    public int compare(Livre l1, Livre l2) {
                        if (l1 == null) {
                            if (l2 != null) return -1;
                        }
                        if (l2 == null) return 1;
                        int result = l1.getTitre().compareTo(l2.getTitre());
                        if (result == 0) result = l1.getTitre().compareTo(l2.getTitre());
                        return result;
                    }
                });
                listItem = new ArrayList<HashMap<String, String>>();
                for(Livre l : biblio){
                    map = new HashMap<String, String>();
                    map.put("Titre", l.getTitre());
                    map.put("Auteur", l.getAuteur());
                    listItem.add(map);
                }

                mSchedule = new MyListAdapter(this, listItem,
                        R.layout.listecontent, new String[]{"Titre", "Auteur"}, new int[]{
                        R.id.textViewTitre, R.id.textViewAuteur
                });
                listView.setAdapter(mSchedule);
                break;
            case R.id.buttonByAuteur:
                Collections.sort(biblio, new Comparator<Livre>() {
                    @Override
                    public int compare(Livre l1, Livre l2) {
                        if (l1 == null) {
                            if (l2 != null) return -1;
                        }
                        if (l2 == null) return 1;
                        int result = l1.getAuteur().compareTo(l2.getAuteur());
                        if (result == 0) result = l1.getAuteur().compareTo(l2.getAuteur());
                        return result;
                    }
                });
                listItem = new ArrayList<HashMap<String, String>>();
                for(Livre l : biblio){
                    map = new HashMap<String, String>();
                    map.put("Titre", l.getTitre());
                    map.put("Auteur", l.getAuteur());
                    listItem.add(map);
                }

                mSchedule = new MyListAdapter(this, listItem,
                        R.layout.listecontent, new String[]{"Titre", "Auteur"}, new int[]{
                        R.id.textViewTitre, R.id.textViewAuteur
                });
                listView.setAdapter(mSchedule);
                break;
            case R.id.buttonByIsbn:
                Collections.sort(biblio, new Comparator<Livre>() {
                    @Override
                    public int compare(Livre l1, Livre l2) {
                        if (l1 == null) {
                            if (l2 != null) return -1;
                        }
                        if (l2 == null) return 1;
                        int result = l1.getIsbn().compareTo(l2.getIsbn());
                        if (result == 0) result = l1.getIsbn().compareTo(l2.getIsbn());
                        return result;
                    }
                });
                listItem = new ArrayList<HashMap<String, String>>();
                for(Livre l : biblio){
                    map = new HashMap<String, String>();
                    map.put("Titre", l.getTitre());
                    map.put("Auteur", l.getAuteur());
                    listItem.add(map);
                }

                mSchedule = new MyListAdapter(this, listItem,
                        R.layout.listecontent, new String[]{"Titre", "Auteur"}, new int[]{
                        R.id.textViewTitre, R.id.textViewAuteur
                });
                listView.setAdapter(mSchedule);
                break;
        }
    }

    private void openBD(){ bd.open(); }
    private void closeBD(){ bd.close(); }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        biblio = getBiblio();
        listItem = new ArrayList<HashMap<String, String>>();
        for(Livre l : biblio){
            map = new HashMap<String, String>();
            map.put("Titre", l.getTitre());
            map.put("Auteur", l.getAuteur());
            listItem.add(map);
        }
        mSchedule = new MyListAdapter(this, listItem,
                R.layout.listecontent, new String[]{"Titre", "Auteur"}, new int[]{
                R.id.textViewTitre, R.id.textViewAuteur
        });
        listView.setAdapter(mSchedule);
    }

    private ArrayList<Livre> getBiblio(){
        openBD();
        ArrayList<Livre> res = bd.getBiblio();
        closeBD();
        return res;
    }

    class MyListAdapter extends SimpleAdapter {

        private LayoutInflater mInflater;

        public MyListAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public Object getItem (int position)
        {
            return super.getItem(position);
        }

        @Override
        public View getView (int position, View convertView, ViewGroup parent) {
            convertView = mInflater.inflate(R.layout.listecontent, null);

            if( position%2 == 0){
                ((TextView) convertView.findViewById(R.id.textViewTitleAuteur)).setTextColor(
                        convertView.getResources().getColor(R.color.red));
                ((TextView) convertView.findViewById(R.id.textViewAuteur)).setTextColor(
                        convertView.getResources().getColor(R.color.red));
                ((TextView) convertView.findViewById(R.id.textViewTitleTitre)).setTextColor(
                        convertView.getResources().getColor(R.color.red));
                ((TextView) convertView.findViewById(R.id.textViewTitre)).setTextColor(
                        convertView.getResources().getColor(R.color.red));
            }
            return super.getView(position, convertView, parent);
        }
    }
}
